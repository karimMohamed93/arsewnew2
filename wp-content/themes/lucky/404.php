<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area 404-default single-content">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<?php
				$page_id = s7upf_get_option('s7upf_404_page');
				if(!empty($page_id)) {
				    echo        	S7upf_Template::get_vc_pagecontent($page_id);
				}
				else{ ?>
				<section class="error-404 not-found">
					<header class="page-header">
						<h2 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'lucky' ); ?></h2>
					</header><!-- .page-header -->

					<div class="page-content">
						<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'lucky' ); ?></p>

						<?php get_search_form(); ?>
					</div><!-- .page-content -->
				</section><!-- .error-404 -->
				<?php } ?>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
