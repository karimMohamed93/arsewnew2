<?php
/**
 * The template for displaying all single posts.
 *
 * @package 7up-framework
 */
?>
<?php get_header();?>
    <div id="main-content"  class="main-wrapper single-content">
        <div class="content-single">
            <div class="container">
                <?php s7upf_display_breadcrumb();?>
                <div class="row">
                    <?php s7upf_output_sidebar('left')?>
                    <div class="<?php echo esc_attr(s7upf_get_main_class()); ?>">
                        <?php
                        while ( have_posts() ) : the_post();

                            /*
                            * Include the post format-specific template for the content. If you want to
                            * use this in a child theme, then include a file called called content-___.php
                            * (where ___ is the post format) and that will be used instead.
                            */
                            get_template_part( 's7upf_templates/single-content/content',get_post_format() );
                            wp_link_pages( array(
                                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'lucky' ),
                                'after'  => '</div>',
                                'link_before' => '<span>',
                                'link_after'  => '</span>',
                            ) );
                            ?>
                            <div class="list-post-tags">
                                <label><?php esc_html_e("Tags","lucky")?>:</label>
                                <?php echo get_the_tag_list(' ',' ',' ')?>
                            </div>
                            <div class="control-post">
                                <div class="row">
                                    <?php
                                    $previous_post = get_previous_post();
                                    $next_post = get_next_post();
                                    ?>
                                    <div class="col-md-6 col-sm-6 col-xs-6 text-left">
                                        <?php if(!empty( $previous_post )):?>
                                            <a href="<?php echo get_permalink( $previous_post->ID ); ?>" class="prev-post"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> <?php esc_html_e("Prev","lucky")?></a>
                                            <h3 class="post-title hidden-xs"><a href="<?php echo get_permalink( $previous_post->ID ); ?>"><?php echo balanceTags($previous_post->post_title); ?></a></h3>
                                        <?php endif;?>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                        <?php if(!empty( $next_post )):?>
                                            <a href="<?php echo get_permalink( $next_post->ID ); ?>" class="next-post"><?php esc_html_e("Next","lucky")?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                            <h3 class="post-title hidden-xs"><a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo balanceTags($next_post->post_title); ?></a></h3>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if ( comments_open() || get_comments_number() ) { comments_template(); }
                           
                        endwhile; ?>
                    </div>
                    <?php s7upf_output_sidebar('right')?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>