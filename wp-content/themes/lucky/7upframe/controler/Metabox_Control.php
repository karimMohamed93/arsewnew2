<?php
/**
 * Created by Sublime Text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:20 AM
 */

add_action('admin_init', 's7upf_custom_meta_boxes');
if(!function_exists('s7upf_custom_meta_boxes')){
    function s7upf_custom_meta_boxes(){
        //Format content
        $format_metabox = array(
            'id' => 'block_format_content',
            'title' => esc_html__('Post Settings', 'lucky'),
            'desc' => '',
            'pages' => array('post'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(                
                array(
                    'id' => 'format_image',
                    'label' => esc_html__('Upload Image', 'lucky'),
                    'type' => 'upload',
                ),
                array(
                    'id' => 'format_gallery',
                    'label' => esc_html__('Add Gallery', 'lucky'),
                    'type' => 'Gallery',
                ),
                array(
                    'id' => 'format_media',
                    'label' => esc_html__('Link Media', 'lucky'),
                    'type' => 'text',
                ),
                array(
                    'id' => 'short_des',
                    'label' => esc_html__('Short Description', 'lucky'),
                    'type' => 'text',
                ),
                array(
                    'id' => 'first_intro',
                    'label' => esc_html__('First Intro', 'lucky'),
                    'type' => 'Textarea',
                )
            ),
        );
        //Product Add Info Tab
        $product_info_tab = array(
            'id' => 'product_info_content',
            'title' => esc_html__('Add Information Tab', 'lucky'),
            'pages' => array('product'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id' => 'product_information',
                    'label' => esc_html__('Information Content', 'lucky'),
                    'type' => 'Textarea',
                )
            ),
        );
        //Show page title
        $show_page_title = array(
            'id' => 'page_title_setting',
            'title' => esc_html__('Title page setting', 'lucky'),
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id' => 'show_title_page',
                    'label' => esc_html__('Show title', 'lucky'),
                    'type' => 'on-off',
                    'std'   => 'on',
                ),
                array(
                    'id'          => 'main_color',
                    'label'       => esc_html__('Main color','lucky'),
                    'type'        => 'colorpicker',
                ),
                array(
                    'id' => 'shop_ajax',
                    'label' => esc_html__('Shop Ajax', 'lucky'),
                    'type' => 'select',
                    'std'   => '',
                    'choices'     => array(
                        array(
                            'label'=>esc_html__('--Select--','lucky'),
                            'value'=>'',
                        ),
                        array(
                            'label'=>esc_html__('On','lucky'),
                            'value'=>'on'
                        ),
                        array(
                            'label'=>esc_html__('Off','lucky'),
                            'value'=>'off'
                        ),
                    ),
                )
            ),
        );
        $product_trendding = array(
            'id' => 'product_trendding',
            'title' => esc_html__('Product Type', 'lucky'),
            'desc' => '',
            'pages' => array('product'),
            'context' => 'side',
            'priority' => 'high',
            'fields' => array(                
                array(
                    'id'    => 'trending_product',
                    'label' => esc_html__('Product Trendding', 'lucky'),
                    'type'        => 'on-off',
                    'std'         => 'off'
                ),
            ),
        );
        // SideBar
        $sidebar_metabox_default = array(
            'id'        => 's7upf_sidebar_option',
            'title'     => esc_html__('Advanced Settings','lucky'),
            'desc'      => '',
            'pages'     => array( 'page','post','product'),
            'context'   => 'side',
            'priority'  => 'low',
            'fields'    => array(
                array(
                    'id'          => 's7upf_sidebar_position',
                    'label'       => esc_html__('Sidebar position ','lucky'),
                    'type'        => 'select',
                    'std' => '',
                    'choices'     => array(
                        array(
                            'label'=>esc_html__('--Select--','lucky'),
                            'value'=>'',
                        ),
                        array(
                            'label'=>esc_html__('No Sidebar','lucky'),
                            'value'=>'no'
                        ),
                        array(
                            'label'=>esc_html__('Left sidebar','lucky'),
                            'value'=>'left'
                        ),
                        array(
                            'label'=>esc_html__('Right sidebar','lucky'),
                            'value'=>'right'
                        ),
                    ),

                ),
                array(
                    'id'        =>'s7upf_select_sidebar',
                    'label'     =>esc_html__('Selects sidebar','lucky'),
                    'type'      =>'sidebar-select',
                    'condition' => 's7upf_sidebar_position:not(no),s7upf_sidebar_position:not()',
                ),
                array(
                    'id'          => 's7upf_show_breadrumb',
                    'label'       => esc_html__('Show Breadcrumb','lucky'),
                    'type'        => 'select',
                    'choices'     => array(
                        array(
                            'label'=>esc_html__('--Select--','lucky'),
                            'value'=>'',
                        ),
                        array(
                            'label'=>esc_html__('Yes','lucky'),
                            'value'=>'yes'
                        ),
                        array(
                            'label'=>esc_html__('No','lucky'),
                            'value'=>'no'
                        ),
                    ),
                ),
                array(
                    'id'          => 's7upf_breadrumb_style',
                    'label'       => esc_html__('Breadcrumb Style','lucky'),
                    'type'        => 'select',
                    'choices'     => array(
                        array(
                            'label'=>esc_html__('Default','lucky'),
                            'value'=>''
                        ),
                        array(
                            'label'=>esc_html__('Remove border','lucky'),
                            'value'=>'border-none'
                        ),
                    ),

                ),
                array(
                    'id'          => 's7upf_header_page',
                    'label'       => esc_html__('Choose page header','lucky'),
                    'type'        => 'select',
                    'choices'     => s7upf_list_header_page()
                ),
                array(
                    'id'          => 's7upf_footer_page',
                    'label'       => esc_html__('Choose page footer','lucky'),
                    'type'        => 'page-select'
                ),
            )
        );
        // Header append
        $header_append = array(
            'id'        => 's7upf_header_append',
            'title'     => esc_html__('Header Image','lucky'),
            'desc'      => '',
            'pages'     => array( 'page','post','product'),
            'context'   => 'normal',
            'priority'  => 'low',
            'fields'    => array(                
                array(
                    'id'          => 'show_header_page',
                    'label'       => esc_html__('Show Header Image','lucky'),
                    'type'        => 'select',
                    'choices'     => array(
                        array(
                            'label'=>esc_html__('--Select--','lucky'),
                            'value'=>'',
                        ),
                        array(
                            'label'=>esc_html__('Yes','lucky'),
                            'value'=>'on'
                        ),
                        array(
                            'label'=>esc_html__('No','lucky'),
                            'value'=>'off'
                        ),
                    ),
                ),
                array(
                    'id'          => 'header_page_image',
                    'label'       => esc_html__( 'Header Item', 'lucky' ),
                    'type'        => 'list-item',
                    'settings'    => array( 
                        array(
                            'id' => 'header_image',
                            'label' => esc_html__('Image', 'lucky'),
                            'type' => 'upload',
                        ),
                        array(
                            'id' => 'header_link',
                            'label' => esc_html__('Link', 'lucky'),
                            'type' => 'text',
                        ),
                    ),
                    'condition' => 'show_header_page:is(on)',
                )

            )
        );
        $product_custom_tab = array(
            'id' => 'block_product_custom_tab',
            'title' => esc_html__('Product Display', 'lucky'),
            'desc' => '',
            'pages' => array('product'),
            'context' => 'normal',
            'priority' => 'low',
            'fields' => array(
                array(
                    'id'          => 'attribute_style',
                    'label'       => esc_html__('Attribute Style','lucky'),
                    'type'        => 'select',
                    'choices'     => array(
                        array(
                            'value'=> '',
                            'label'=> esc_html__("Default", 'lucky'),
                        ),
                        array(
                            'value'=> 'normal',
                            'label'=> esc_html__("Normal", 'lucky'),
                        ),
                        array(
                            'value'=> 'special',
                            'label'=> esc_html__("Special", 'lucky'),
                        ),
                    )
                ),
                array(
                    'id'          => 'show_single_number',
                    'label'       => esc_html__('Show Single Products Number','lucky'),
                    'type'        => 'text',
                ),
                array(
                    'id'          => 'show_single_lastest',
                    'label'       => esc_html__('Show Single Lastest Products','lucky'),
                    'type'        => 'select',
                    'choices'     => array(
                        array(
                            'value'=> '',
                            'label'=> esc_html__("--Select--", 'lucky'),
                        ),
                        array(
                            'value'=> 'on',
                            'label'=> esc_html__("Yes", 'lucky'),
                        ),
                        array(
                            'value'=> 'off',
                            'label'=> esc_html__("No", 'lucky'),
                        ),
                    )
                ),
                array(
                    'id'          => 'show_single_upsell',
                    'label'       => esc_html__('Show Single Upsell Products','lucky'),
                    'type'        => 'select',
                    'choices'     => array(
                        array(
                            'value'=> '',
                            'label'=> esc_html__("Default", 'lucky'),
                        ),
                        array(
                            'value'=> 'on',
                            'label'=> esc_html__("Yes", 'lucky'),
                        ),
                        array(
                            'value'=> 'off',
                            'label'=> esc_html__("No", 'lucky'),
                        ),
                    )
                ),
                array(
                    'id'          => 'show_single_relate',
                    'label'       => esc_html__('Show Single Relate Products','lucky'),
                    'type'        => 'select',
                    'choices'     => array(
                        array(
                            'value'=> '',
                            'label'=> esc_html__("Default", 'lucky'),
                        ),
                        array(
                            'value'=> 'on',
                            'label'=> esc_html__("Yes", 'lucky'),
                        ),
                        array(
                            'value'=> 'off',
                            'label'=> esc_html__("No", 'lucky'),
                        ),
                    )
                ),
                array(
                    'id'          => 'product_tab_data',
                    'label'       => esc_html__('Custom Tab','lucky'),
                    'type'        => 'list-item',
                    'settings'    => array(
                        array(
                            'id' => 'tab_content',
                            'label' => esc_html__('Content', 'lucky'),
                            'type' => 'textarea',
                        ),
                    )
                ),
            ),
        );
        if (function_exists('ot_register_meta_box')){
            ot_register_meta_box($format_metabox);
            ot_register_meta_box($sidebar_metabox_default);
            ot_register_meta_box($header_append);
            ot_register_meta_box($product_info_tab);
            ot_register_meta_box($show_page_title);
            ot_register_meta_box($product_trendding);
            ot_register_meta_box($product_custom_tab);
        }
    }
}
?>