<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 26/12/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_mailchimp'))
{
    function s7upf_vc_mailchimp($attr)
    {
        $html = $bg_class = '';
        extract(shortcode_atts(array(
            'style'         => '',
            'title'         => '',
            'des'           => '',
            'placeholder'   => '',
            'submit'        => '',
            'form_id'       => '',
        ),$attr));
        $form_html = apply_filters('sv_remove_autofill',do_shortcode('[mc4wp_form id="'.$form_id.'"]'));
        switch ($style) {
            case 'footer-page':
                $html .=    '<div class="newsletter-footer">';
                if(!empty($title)) $html .=    '<h2 class="sub-title">'.esc_html($title).'</h2>';
                if(!empty($des)) $html .=    '<p>'.esc_html($des).'</p>';
                $html .=        $form_html;
                $html .=    '</div>';
                break;
            
            default:
                $html .=    '<div class="newsletter-form '.esc_attr($style).' sv-mailchimp-form" data-placeholder="'.$placeholder.'" data-submit="'.$submit.'">
                                <label>'.esc_html($title).'</label>
                                '.$form_html.'
                            </div>';
                break;
        }        
        return $html;
    }
}

stp_reg_shortcode('sv_mailchimp','s7upf_vc_mailchimp');

vc_map( array(
    "name"      => esc_html__("SV MailChimp", 'lucky'),
    "base"      => "sv_mailchimp",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type" => "dropdown",
            'holder'      => 'div',
            "heading" => esc_html__("Style",'lucky'),
            "param_name" => "style",
            "value"     => array(
                esc_html__("Default",'lucky')     => '',
                esc_html__("Footer Page",'lucky')     => 'footer-page',
                )
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Form ID",'lucky'),
            "param_name" => "form_id",
        ),
        array(
            "type" => "textfield",
            'holder'      => 'div',
            "heading" => esc_html__("Title",'lucky'),
            "param_name" => "title",
        ),
        array(
            "type" => "textfield",
            'holder'      => 'div',
            "heading" => esc_html__("Description",'lucky'),
            "param_name" => "des",
            "dependency"    => array(
                "element"   => "style",
                "value"   => "footer-page",
                )
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Placeholder Input",'lucky'),
            "param_name" => "placeholder",
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Submit Label",'lucky'),
            "param_name" => "submit",
        ),
    )
));