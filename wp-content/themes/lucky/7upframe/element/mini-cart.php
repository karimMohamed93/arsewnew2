<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_mini_cart'))
{
    function s7upf_vc_mini_cart($attr)
    {
        $html = '';
        extract(shortcode_atts(array(
            'style'      => '',
        ),$attr));
        $html .=    '<div class="info-cart mini-cart8 '.$style.'">
                        <a class="mycart-link" href="'.wc_get_cart_url().'"><span class="lnr lnr-cart"></span> <span class="mini-cart-total">'.WC()->cart->get_cart_total().'</span></a>
                        <div class="inner-cart-info">
                            <h2><span class="cart-item-count">0</span> '.esc_html__("items","lucky").'</h2>
                            <div class="mini-cart-content">'.sv_mini_cart().'</div>
                        </div>
                    </div>';
        return apply_filters('s7upf_tempalte_mini_cart',$html);
    }
}

stp_reg_shortcode('sv_mini_cart','s7upf_vc_mini_cart');

vc_map( array(
    "name"      => esc_html__("SV Mini Cart", 'lucky'),
    "base"      => "sv_mini_cart",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            'heading'     => esc_html__( 'Style', 'lucky' ),
            'type'        => 'dropdown',
            'param_name'  => 'style',
            'value'       => array(
                esc_html__('Home 8','lucky') => '',
                esc_html__('Home 9','lucky') => 'mini-cart9',
                )
        ), 
    )
));