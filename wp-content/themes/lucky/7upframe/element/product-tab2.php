<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 05/09/15
 * Time: 10:00 AM
 */
if(class_exists("woocommerce")){
    if(!function_exists('s7upf_vc_product_tab2'))
    {
        function s7upf_vc_product_tab2($attr, $content = false)
        {
            $html = $el_class = $html_wl = $html_cp = '';
            extract(shortcode_atts(array(
                'style'             => '',
                'tabs'              => '',
                'number'            => '',
                'cats'              => '',
                'order_by'          => 'date',
                'order'             => 'DESC',
                'item_style'        => 'list-product1',
                'item'              => '',
                'item_slide'        => '8',
                'item_res'          => '',
                'speed'             => '',
                'size'              => '',
                'animation'         => '',
            ),$attr));            
            $args=array(
                'post_type'         => 'product',
                'posts_per_page'    => $number,
                'orderby'           => $order_by,
                'order'             => $order
            );
            if(!empty($cats)) {
                $custom_list = explode(",",$cats);
                $args['tax_query'][]=array(
                    'taxonomy'=>'product_cat',
                    'field'=>'slug',
                    'terms'=> $custom_list
                );
            }
            $pre = rand(1,100);
            if(!empty($tabs)){
                if(empty($item) && empty($item_res)) $item_res = '0:1';
                if(empty($item)) $item = 1;
                if(empty($size)) $size = array(250,310);
                $item_slide = (int)$item_slide;
                $tabs = explode(',', $tabs);
                $tab_html = $content_html = '';
                foreach ($tabs as $key => $tab) {
                    switch ($tab) {
                        case 'bestsell':
                            $tab_title =    esc_html__("Popular","lucky");
                            $args['meta_key'] = 'total_sales';
                            $args['orderby'] = 'meta_value_num';
                            break;

                        case 'toprate':
                            $tab_title =    esc_html__("Most review","lucky");
                            unset($args['meta_key']);
                            $args['meta_key'] = '_wc_average_rating';
                            $args['orderby'] = 'meta_value_num';
                            $args['meta_query'] = WC()->query->get_meta_query();
                            $args['tax_query'][] = WC()->query->get_tax_query();
                            break;
                        
                        case 'mostview':
                            $tab_title =    esc_html__("Most View","lucky");
                            unset($args['no_found_rows']);
                            unset($args['meta_query']);
                            unset($args['tax_query']);
                            if(!empty($cats)) {
                                $custom_list = explode(",",$cats);
                                $args['tax_query'][]=array(
                                    'taxonomy'=>'product_cat',
                                    'field'=>'slug',
                                    'terms'=> $custom_list
                                );
                            }
                            $args['meta_key'] = 'post_views';
                            $args['orderby'] = 'meta_value_num';
                            break;

                        case 'featured':
                            $tab_title =    esc_html__("Featured","lucky");
                            $args['orderby'] = $order_by;
                            $args['meta_key'] = '_featured';
                            $args['meta_value'] = 'yes';
                            break;

                        case 'trendding':
                            unset($args['meta_key']);
                            unset($args['meta_value']);
                            $tab_title =    esc_html__("Trending","lucky");
                            $args['meta_query'][] = array(
                                'key'     => 'trending_product',
                                'value'   => 'on',
                                'compare' => '=',
                            );
                            break;
                        
                        case 'onsale':
                            $tab_title =    esc_html__("On sale","lucky");
                            unset($args['meta_query']);
                            unset($args['meta_key']);
                            unset($args['meta_value']);
                            $args['meta_query']['relation']= 'OR';
                            $args['meta_query'][]=array(
                                'key'   => '_sale_price',
                                'value' => 0,
                                'compare' => '>',                
                                'type'          => 'numeric'
                            );
                            $args['meta_query'][]=array(
                                'key'   => '_min_variation_sale_price',
                                'value' => 0,
                                'compare' => '>',                
                                'type'          => 'numeric'
                            );
                            break;
                        
                        default:
                            $tab_title =    esc_html__("New arrivals","lucky");
                            $args['orderby'] = 'date';
                            break;
                    }
                    if($key == 0) $f_class = 'active';
                    else $f_class = '';
                    $product_query = new WP_Query($args);
                    $count = 1;
                    $count_query = $product_query->post_count;
                    $max_page = $product_query->max_num_pages;
                    $data_load['product_type'] = $tab;
                    $data_loadjs = json_encode($data_load);
                    $tab_html .=    '<li class="'.$f_class.'"><a href="'.esc_url('#'.$pre.$tab).'" data-toggle="tab">'.$tab_title.'</a></li>';
                    $content_html .=    '<div id="'.$pre.$tab.'" class="tab-pane clearfix '.$f_class.'">
                                            <div class="paginumber-slider">
                                                <div class="wrap-item smart-slider" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="" data-next="" data-paginumber="true" data-pagination="true" data-navigation="">';
                    if($product_query->have_posts()) {
                        while($product_query->have_posts()) {
                            $product_query->the_post();
                            global $product;
                            if($count % $item_slide == 1) $content_html .=    '<div class="'.esc_attr($item_style).'"><div class="row">';
                            switch ($item_style) {
                                case 'list-product4':
                                    $check = rand(0,10);
                                    if($check % 2 == 0) $size = array(270,236);
                                    else $size = array(270,335);
                                    $content_html .=    '<div class="item-product-masonry">
                                                            <div class="product-thumb">
                                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                                <div class="product-info">
                                                                    '.s7upf_product_link().'
                                                                    '.s7upf_get_rating_html().'
                                                                    <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                                    '.s7upf_get_price_html().'
                                                                </div>
                                                            </div>
                                                        </div>';
                                    break;
                                
                                default:
                                    $content_html .=    '<div class="col-md-3 col-sm-4 col-xs-6">
                                                            <div class="item-product">
                                                                <div class="product-thumb">
                                                                    <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                                    '.s7upf_product_link().'
                                                                    <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                                    '.s7upf_get_rating_html().'
                                                                    '.s7upf_get_saleoff_html().'
                                                                </div>
                                                                <div class="product-info">
                                                                    <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                                    '.s7upf_get_price_html().'
                                                                </div>
                                                            </div>
                                                        </div>';
                                    break;
                            }
                            if($count % $item_slide == 0 || $count == $count_query) $content_html .=    '</div></div>';
                            $count++;
                        }
                    }
                    $content_html .=            '</div>
                                            </div>
                                        </div>';
                }
                
                $html .=    '<div class="product-tab-paginav">
                                <div class="title-tab2 text-center">
                                    <ul class="list-none">
                                        '.$tab_html.'
                                    </ul>';                
                $html .=        '</div>
                                <div class="tab-content">
                                    '.$content_html.'
                                </div>
                            </div>';
            }
            wp_reset_postdata();
            return $html;
        }
    }

    stp_reg_shortcode('sv_product_tab2','s7upf_vc_product_tab2');
    add_action( 'vc_before_init_base','s7upf_add_product_tab2',10,100 );
    if ( ! function_exists( 's7upf_add_product_tab2' ) ) {
        function s7upf_add_product_tab2(){
            vc_map( array(
                "name"      => esc_html__("SV Product Tab 2", 'lucky'),
                "base"      => "sv_product_tab2",
                "icon"      => "icon-st",
                "category"  => '7Up-theme',
                "params"    => array(
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__("Style",'lucky'),
                        "param_name" => "style",
                        "value"     => array(
                            esc_html__("Default",'lucky')   => '',
                            )
                    ),
                    array(
                        'heading'     => esc_html__( 'Number', 'lucky' ),
                        'type'        => 'textfield',
                        'description' => esc_html__( 'Enter number of product. Default is 10.', 'lucky' ),
                        'param_name'  => 'number',
                    ),
                    array(
                        'holder'     => 'div',
                        'heading'     => esc_html__( 'Product Categories', 'lucky' ),
                        'type'        => 'checkbox',
                        'param_name'  => 'cats',
                        'value'       => s7upf_list_taxonomy('product_cat',false)
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Order By', 'lucky' ),
                        'value' => s7upf_get_order_list(),
                        'param_name' => 'orderby',
                        'description' => esc_html__( 'Select Orderby Type ', 'lucky' ),
                        'edit_field_class'=>'vc_col-sm-6 vc_column',
                    ),
                    array(
                        'heading'     => esc_html__( 'Order', 'lucky' ),
                        'type'        => 'dropdown',
                        'param_name'  => 'order',
                        'value' => array(                   
                            esc_html__('Desc','lucky')  => 'DESC',
                            esc_html__('Asc','lucky')  => 'ASC',
                        ),
                        'description' => esc_html__( 'Select Order Type ', 'lucky' ),
                        'edit_field_class'=>'vc_col-sm-6 vc_column',
                    ),
                    array(
                        "type" => "checkbox",
                        "heading" => esc_html__("Tabs",'lucky'),
                        "param_name" => "tabs",
                        "value" => array(
                            esc_html__("New Arrivals",'lucky')    => 'newarrival',
                            esc_html__("Best Seller",'lucky')     => 'bestsell',
                            esc_html__("Most Review",'lucky')     => 'toprate',
                            esc_html__("Most View",'lucky')       => 'mostview',
                            esc_html__("Featured",'lucky')        => 'featured',
                            esc_html__("Trendding",'lucky')       => 'trendding',
                            esc_html__("On Sale",'lucky')         => 'onsale',
                            ),
                    ),                    
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__("Item style",'lucky'),
                        "param_name" => "item_style",
                        "value"     => array(
                            esc_html__("Box shadow",'lucky')   => 'list-product1',
                            esc_html__("Full",'lucky')   => 'list-product2',
                            esc_html__("Masonry",'lucky')   => 'list-product4',
                            )
                    ),
                    array(
                        "type"          => "textfield",
                        "heading"       => esc_html__("Size Thumbnail",'lucky'),
                        "param_name"    => "size",
                        "group"         => esc_html__("Advanced",'lucky'),
                        'description' => esc_html__( 'Enter site thumbnail to crop. [width]x[height]. Example is 300x300', 'lucky' ),
                    ),
                    array(
                        "type"          => "textfield",
                        "heading"       => esc_html__("Item",'lucky'),
                        "param_name"    => "item",
                        "group"         => esc_html__("Advanced",'lucky'),
                    ),
                    array(
                        "type"          => "textfield",
                        "heading"       => esc_html__("Item / Slide",'lucky'),
                        "param_name"    => "item_slide",
                        "group"         => esc_html__("Advanced",'lucky'),
                    ),
                    array(
                        "type"          => "textfield",
                        "heading"       => esc_html__("Item Responsive",'lucky'),
                        "param_name"    => "item_res",
                        "group"         => esc_html__("Advanced",'lucky'),
                        'description' => esc_html__( 'Enter item for screen width(px) format is width:value and separate values by ",". Example is 0:2,600:3,1000:4. Default is auto.', 'lucky' ),
                    ),
                    array(
                        "type"          => "textfield",
                        "heading"       => esc_html__("Speed",'lucky'),
                        "param_name"    => "speed",
                        "group"         => esc_html__("Advanced",'lucky'),                    
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Animation', 'lucky' ),
                        'param_name' => 'animation',
                        'value' => s7upf_get_list_animation()
                    ),
                )
            ));
        }
    }
    
}
