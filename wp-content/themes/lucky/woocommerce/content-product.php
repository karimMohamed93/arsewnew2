<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;
// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;
?>
<?php
	$type = 'grid';
    if(isset($_GET['type'])){
        $type = $_GET['type'];
    }
?>
<?php if($type == 'list'){?>
	<li class="col-md-12 col-sm-12 col-xs-12">	
		<?php
			echo '<div class="item-product clearfix">
						<div class="product-thumb">
	                        <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),array(250,310)).'</a>
	                        '.s7upf_product_link().'
	                        <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
	                    </div>
						<div class="product-info">
							<h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
							'.s7upf_get_rating_html().'
							<p class="desc">'.get_the_excerpt().'</p>
							<div class="wrap-cart-qty">
								'.s7upf_get_price_html().'
								<div class="info-extra product">
									'.s7upf_wishlist_link().'
									'.s7upf_compare_url().'
								</div>';
								woocommerce_template_single_add_to_cart();
			echo 			'</div>
						</div>
					</div>';
		?>
	</li>
<?php }
	else{
		$col_option = $woocommerce_loop['columns'];
		switch ($col_option) {
			case 1:
				$size = 'full';
				$col = 'col-md-12 col-sm-12';
				break;

			case 2:
				$size = array(250*1.5,310*1.5);
				$col = 'col-md-6 col-sm-6';
				break;
			
			case 3:
				$size = array(250*1.2,310*1.2);
				$col = 'col-md-4 col-sm-6';
				break;

			case 6:
				$col = 'col-md-2 col-sm-3';
				$size = array(250,310);
				break;

			default:
				$col = 'col-md-3 col-sm-4';
				$size = array(250,310);
				break;
		}
		$custom_size = s7upf_get_option('shop_custom_size_thumb');
		if(!empty($custom_size)) $size = explode('x', $custom_size)
	?>
	<li class="<?php echo esc_attr($col)?> col-xs-6">
		<?php
			echo '<div class="item-product">
                    <div class="product-thumb">
                        <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),'full').'</a>
                        '.s7upf_product_link().'
                        <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                        '.s7upf_get_rating_html().'
                        '.s7upf_get_saleoff_html().'
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                        '.s7upf_get_price_html().'
                    </div>
                </div>';
		?>
	</li>	
<?php }?>